<?php
/**
 * Module administrative internals.
 *
 * @file
 *  Administrative UI for the ANU Remote Styles Cache 
 *
 * @author Taihao Zhang (zhang.taihao@gmail.com)
 * @since 6.x-1.0
 */

/**
 * Caching task under ANU site manager configuration.
 * @since 6.x-1.0
 */
function anu_rs_cache_caching_page() {
  $output = '';

  // Flush cache
  $output .= theme_fieldset(
    array(
      '#title' => t('Flush disc cache'),
      '#value' => '<p>'. t('Refresh remote styles disc cache, if possible.') .'</p>'.
        '<p>'. l(t('Flush'), 'admin/settings/anu-site-manager/disc-cache/flush'). '</p>',
    )
  );

  return $output;
}

/**
 * Add remote styles caching settings to ANU site manager settings form.
 * @since 6.x-1.0
 */
function anu_rs_cache_form_anu_site_manager_settings_form_alter(&$form) {
  $form['anu_rs_cache'] = array(
    '#title' => t('Disc caching'),
    '#description' => t('Configure remote styles disc cache settings.'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#weight' => 2,
  );

  $form['anu_rs_cache']['lifetime'] = array(
    '#title' => t('Cache minimum lifetime'),
    '#description' => t('Number of seconds the disc cache should last until the next expiry time.'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#size' => 10,
    '#default_value' => variable_get('anu_rs_cache_lifetime', ANU_RS_CACHE_LIFETIME),
  );

  /*
  $form['anu_rs_cache']['expiry_time'] = array(
    '#title' => t('Cache expiry time'),
    '#description' => t('The time of day at which cache will expire and next request refreshes the cache automatically. The format is %format in 24-hour scheme. Alternatively, to make cache expire immediately after minimum lifetime, leave it blank.', array('%format' => 'HHMM')),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => variable_get('anu_rs_cache_expiry_time', ''),
  );
  */

  $form['submit']['#weight'] = 10;

  $form['#validate'][] = 'anu_rs_cache_settings_form_validate';
  $form['#submit'][] = 'anu_rs_cache_settings_form_submit';
}

/**
 * ANU site manager settings form validate handler.
 * @since 6.x-1.0
 */
function anu_rs_cache_settings_form_validate($form, &$form_state) {
  $values = $form_state['values']['anu_rs_cache'];

  // Non-negative cache life time
  if ($values['lifetime'] != '' && !ctype_digit($values['lifetime'])) {
    form_set_error('anu_rs_cache][lifetime', t('Please enter a non-negative integer for cache lifetime.'));
  }

  /*
  // Cache expiry time
  $expiry_time = $values['expiry_time'];
  if (trim($expiry_time) != '' && !preg_match('/^([0-1][0-9]|2[0-333])([0-5][0-9]|60)$/', $expiry_time)) {
    form_set_error('anu_rs_cache][expiry_time', t('Please enter a valid expiry time.'));
  }
  */
}

/**
 * ANU site manager settings form submit handler.
 * @since 6.x-1.0
 */
function anu_rs_cache_settings_form_submit($form, &$form_state) {
  $values = $form_state['values']['anu_rs_cache'];

  variable_set('anu_rs_cache_lifetime', (int) $values['lifetime']);
  // variable_set('anu_rs_cache_expiry_time', $values['expiry_time']);

  drupal_set_message(t('Caching settings have been saved.'));
}
